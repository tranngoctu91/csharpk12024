﻿// See https://aka.ms/new-console-template for more information
using LearnOOP2.Ex1;
using LearnOOP2.Students;

Console.WriteLine("Hello, World!");
// lớp học học sinh có 2 môn chính là văn,toán, mỗi bạn có thể học thêm 1 môn năng khiếu
var anna = new EngStudent(1, "anna", 7, 5, 6);
var sophia = new EngStudent(2, "sophia", 8, 10, 6);
var bob = new HisStudent(3, "bob", 5, 1, 6);
var tom = new HisStudent(4, "tom", 7, 8, 4);

var students = new List<IGetTotalScore>() { anna, sophia, bob, tom };

foreach (var student in students)
{
	//if (student is HisStudent his)
	//{
	//       Console.WriteLine($"Name: {his.Name} - History Score: {his.His}");
	//   }

	//C1
	//if (student is BaseStudent baseStudent)
	//{
	//	Console.WriteLine(baseStudent.GetInfo());
	//}

	//C2
	var baseStudent1 = student as BaseStudent;
	Console.WriteLine(baseStudent1.GetInfo());
	Console.WriteLine($"total score: {student.GetTotalScore()}");

}

//C3
var student1s = new List<BaseStudent>() { anna, sophia, bob, tom };

foreach (var item in student1s)
{
	if (item is IGetTotalScore studentGetTotlaScore)
	{
		Console.WriteLine($"total score: {studentGetTotlaScore.GetTotalScore()}");
	}
}

Ex1Manager.Init();	
Console.ReadKey();
// tạo ra 1 list student và in ra thông tin
// nếu học sinh đó là HisStudent thì in ra điểm sử
//inra thông thông tin và điểm tổng kết
