﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnOOP2.Students
{
	public abstract class BaseStudent
	{
		public BaseStudent(int id, string name, double math, double lit)
		{
			Id = id;
			Name = name;
			Math = math;
			Lit = lit;
		}
		public int Id { get; set; }
		public string Name { get; set; }
		public double Math { get; set; }
		public double Lit { get; set; }

		public virtual string GetInfo()
		{
			return $"Id: {Id} - Name: {Name} - Avg: {GetAvg()}  Math: {Math} Lit: {Lit}";
		}
		protected abstract double GetAvg();
	}
}
