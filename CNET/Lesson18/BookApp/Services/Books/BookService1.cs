﻿using BookApp.Entities;
using BookApp.Repositories;
using BookApp.UnitOfWork;
using Ex1RepositoryUOW.Entities;
using Microsoft.EntityFrameworkCore;

namespace BookApp.Services.Books
{
    public class BookService1 : IBookService
    {
        private readonly ApplicationDbContext _context;
        public BookService1(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task Create(CreateBookViewModel model)
        {
            var book = new Book()
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                AuthorId = model.AuthorId
            };
            _context.Books.Add(book);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                throw new Exception("Book not found");
            }

            _context.Remove(book);
            await _context.SaveChangesAsync();
        }

        public async Task<List<BookViewModel>> GetAll()
        {
            //var books = _bookRepository.FindAll(null, s => s.Author).Select(
            //    s => new BookViewModel()
            //    {
            //        Id = s.Id,
            //        Name = s.Name,
            //        AuthorName = s.Author.Name
            //    });
            //var result = await books.ToListAsync();
            //return result;

            var books = _context.Books.Include(b => b.Author).Select(
                s => new BookViewModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    AuthorName = s.Author.Name
                });

            var result = await books.ToListAsync();
            return result;
        }

        public async Task<BookViewModel> GetById(Guid id)
        {
            var book = await _context.Books.Include(s => s.Author).FirstOrDefaultAsync(s => s.Id == id);
            if (book == null)
            {
                throw new Exception("Book not found");
            }

            var result = new BookViewModel()
            {
                Id = book.Id,
                Name = book.Name,
                AuthorName = book.Author.Name,
                AuthorId = book.AuthorId
            };
            return result;
        }

        public async Task Update(UpdateBookViewModel model)
        {
            var book = await _context.Books.FindAsync(model.Id);
            if (book == null)
            {
                throw new Exception("Book not found");
            }
            book.Name = model.Name;
            book.AuthorId = model.AuthorId;
            _context.Books.Update(book);
            await _context.SaveChangesAsync();
        }
    }
}
