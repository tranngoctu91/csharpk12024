﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace LearnSwitchCase
{
	public class Demo
	{
		//trả ra message tương ứng với lỗi
		//401 =>unauthorized
		//200 =>OK
		//404 =>not found
		//400 => bad request
		public string ShowMessage(int statusCode)
		{

			var message = string.Empty;
			switch (statusCode)
			{
				case 401:
					message = CommonConstants.UnAuthoried;
					break;
				case 200:
					message = CommonConstants.OK;
					break;
				case 404:
					message = CommonConstants.NotFound;
					break;
				case 400:
					message = CommonConstants.BadRequest;
					break;
				default:
					message = CommonConstants.Undefined;
					break;
			}

			return message;
		}

		// Viết hàm nhận vào Month(int) và Year(int) và trả ra số ngày trong tháng (có xét đến năm nhuận)
		public bool IsLeapYear(int year)
		{
			return (year % 4 == 0 && year % 100 != 0) || (year % 100 == 0 && year % 400 == 0);
		}

		public int GetDaysInMonth(int month, int year)
		{
			if (year < 0)
			{
				return 0;
			}
			switch (month)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					return 31;
				case 4:
				case 6:
				case 9:
				case 11:
					return 30;
				case 2:
					if (IsLeapYear(year))
					{
						return 29;
					}
					else
					{
						return 28;
					}
				default:
					return 0;
			}
		}


		public int GetDaysInMonth1(int month, int year)
		{
			if (year < 0)
			{
				throw new Exception(string.Format(CommonConstants.ErrorMessages.ValueIsNotValid, year));
			}
			switch (month)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					return 31;
				case 4:
				case 6:
				case 9:
				case 11:
					return 30;
				case 2:
					if (IsLeapYear(year))
					{
						return 29;
					}
					else
					{
						return 28;
					}
				default:
					throw new Exception(string.Format(CommonConstants.ErrorMessages.OutOfRangeMessage, month, Month.January.ToString(), Month.December.ToString()));
			}
		}

		public int GetDaysInMonth2(Month month, int year)
		{
			if (year < 0)
			{
				throw new Exception("year <0");
			}
			switch (month)
			{
				case Month.January:
				case Month.March:
				case Month.May:
				case Month.July:
				case Month.August:
				case Month.Octorber:
				case Month.December:
					return 31;
				case Month.April:
				case Month.June:
				case Month.September:
				case Month.November:
					return 30;
				case Month.February:
					if (IsLeapYear(year))
					{
						return 29;
					}
					else
					{
						return 28;
					}
				default:
					throw new Exception("month is out of 1-12");
			}
		}
	}
}
