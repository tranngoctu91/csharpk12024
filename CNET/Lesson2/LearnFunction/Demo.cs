﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace LearnFunction
{
    public class Demo
    {
        public double CalRec(double width, double length)
        {
            double area = width * length;
            return area;
        }

        public void RunCalRec()
        {
            Console.WriteLine("Input the width");
            double.TryParse(Console.ReadLine(), out double width);
            Console.WriteLine("Input the length");

            try
            {
                double length = double.Parse(Console.ReadLine());
                var result = CalRec(width, length);
                Console.WriteLine($"The area: {result}");
            }
            catch (Exception)
            {
                Console.WriteLine("length is not correct");
            }

        }

        public double CalAreaCircle(double radius, out double perimeter)
        {
            double PI = 3.14;
            double area = radius * radius * PI;
            perimeter = radius * 2 * PI;
            return area;
        }

        public void RunCalAreaCircle()
        {
            Console.WriteLine("Input the radius: ");
            double.TryParse(Console.ReadLine(), out double radius);

            var result = CalAreaCircle(radius, out double perimeterResult);
            Console.WriteLine($"The Area of a circle: {result},perimeter: {perimeterResult}");

            var result1 = CalAreaCircle(radius, out double _);
            Console.WriteLine($"The Area of a circle: {result}");
        }

        public void ChangeValue(int number)
        {
            number += 5;
        }

        public void ChangeValueRef(ref int number)
        {
            number += 5;
        }

    }
}
