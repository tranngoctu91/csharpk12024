﻿using Demo.Domain;
using Demo.Domain.ApplicationServices.Orders;
using Demo.Domain.ApplicationServices.Users;
using Demo.Domain.Entities;
using Demo.Domain.Enums;
using Demo.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Demo.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Order, Guid> _orderRepository;
        private readonly IGenericRepository<CartItem, Guid> _cartItemRepository;
        private readonly IGenericRepository<OrderDetail, Guid> _orderDetailRepository;
        private readonly ILogger<OrderService> _logger;

        public OrderService(IUnitOfWork unitOfWork,
            IGenericRepository<Order, Guid> orderRepository,
            IGenericRepository<OrderDetail, Guid> orderDetailRepository,
            IGenericRepository<CartItem, Guid> cartItemRepository, ILogger<OrderService> logger)
        {
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _cartItemRepository = cartItemRepository;
            _logger = logger;
        }

        public async Task<ResponseResult> PlaceOrder(CreateOrderViewModel model, UserProfileModel currentUser)
        {
            var cartItems = await _cartItemRepository.FindAll(s => s.UserId == currentUser.UserId).ToListAsync();
            if (cartItems == null || !cartItems.Any())
            {
                throw new CartException.CartEmptyException();
            }

            await _unitOfWork.BeginTransactionAsync();

            var order = new Order
            {
                Id = Guid.NewGuid(),
                ShippingAddress = model.ShippingAddress,
                TotalAmount = cartItems.Sum(s => s.PurchasePrice * s.Quantity),
                OrderStatus = OrderStatus.New,
                PaymentMethod = model.PaymentMethod,
                CreatedBy = currentUser.UserId,
                CreatedDate = DateTime.Now
            };
            try
            {
                _orderRepository.Add(order);
                await _unitOfWork.SaveChangesAsync();

                var orderDetails = new List<OrderDetail>();
                foreach (var item in cartItems)
                {
                    var orderDetail = new OrderDetail()
                    {
                        Id = Guid.NewGuid(),
                        OrderId = order.Id,
                        PurchasePrice = item.PurchasePrice,
                        UserId = currentUser.UserId,
                        VariantName = item.VariantName,
                        CreatedBy = currentUser.UserId,
                        CreatedDate = DateTime.Now
                    };
                    orderDetails.Add(orderDetail);
                   
                }
                _orderDetailRepository.AddRange(orderDetails);
                await _unitOfWork.SaveChangesAsync();

                _cartItemRepository.RemoveMultiple(cartItems);
                await _unitOfWork.SaveChangesAsync();
                await _unitOfWork.CommitAsync();
                return ResponseResult.Success("Checkout successfully");

            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                await _unitOfWork.RollbackAsync();
                throw new OrderException.ProcessCheckoutException();
            }
        }

        public async Task<PageResult<OrderViewModel>> GetOrders(OrderSearchQuery query)
        {
            var result = new PageResult<OrderViewModel>()
            {
                CurrentPage = query.PageIndex
            };
            var orderQuery = _orderRepository.FindAll(null, s => s.OrderDetails);
            if (query.UserId.HasValue)
            {
                orderQuery = orderQuery.Where(s => s.UserId == query.UserId);
            }

            result.TotalCount = await orderQuery.CountAsync();
            result.Data = await orderQuery.Skip(query.SkipNo).Take(query.TakeNo).Select(s => new OrderViewModel()
            {
                OrderId = s.Id,
                TotalAmount = s.TotalAmount,
                OrderStatus = s.OrderStatus,
                PaymentMethod = s.PaymentMethod,
                OrderDetails = s.OrderDetails.Select(x => new OrderDetailViewModel()
                {
                    OrderDetailId = x.Id,
                    PurchasePrice = x.PurchasePrice,
                    Quantity = x.Quantity,
                    VariantName = x.VariantName
                }).ToList()
            }).ToListAsync();
            return result;
        }

        public async Task<ResponseResult> UpdateOrderStatus(UpdateOrderStatusViewModel model)
        {
            var item = await _orderRepository.FindByIdAsync(model.OrderId);
            if (item == null)
            {
                throw new CategoryException.CategoryNotFoundException(model.OrderId);
            }
            item.OrderStatus = model.OrderStatus;
            try
            {
                _orderRepository.Update(item);
                await _unitOfWork.SaveChangesAsync();
                return ResponseResult.Success();
            }
            catch (Exception e)
            {
               _logger.LogError(e.Message, e);
               throw new OrderException.UpdateOrderException(model.OrderId);
            }
           
        }
    }
}
