﻿using Demo.Domain;
using Demo.Domain.ApplicationServices.Images;
using Demo.Domain.ApplicationServices.Users;
using Demo.Domain.ApplicationServices.Variants;
using Demo.Domain.Entities;
using Demo.Domain.Exceptions;
using Demo.Domain.Utility;
using DemoApp.Domain.Models.Variants;
using DemoApp.Domain.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Demo.Application.Services;

public class VariantService : IVariantService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IGenericRepository<Variant, Guid> _variantRepository;
    private readonly IGenericRepository<GeneralImage, Guid> _generalImageRepository;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private  readonly  ILogger<VariantService> _logger;
    public VariantService(
        IUnitOfWork unitOfWork,
        IGenericRepository<Variant, Guid> variantRepository,
        IGenericRepository<GeneralImage, Guid> generalImageRepository,
        IHttpContextAccessor httpContextAccessor, ILogger<VariantService> logger)
    {
        _unitOfWork = unitOfWork;
        _variantRepository = variantRepository;
        _generalImageRepository = generalImageRepository;
        _httpContextAccessor = httpContextAccessor;
        _logger = logger;
    }

    public async Task<ResponseResult> CreateVariant(CreateVariantViewModel model, UserProfileModel currentUser)
    {
        var listImages = await _generalImageRepository.FindAll(s => model.ImageIds.Contains(s.Id)).Select(s =>
            new ImageInEntity(s.Id, s.Url, s.Name)).ToListAsync();
           
        try
        {
            var variant = new Variant()
            {
                Id = Guid.NewGuid(),
                Name = model.VariantName,
                Price = model.Price,
                ImageJson = HelperUtility.SerializeObject(listImages),
                DiscountPrice = model.DiscountPrice,
                CreatedBy = currentUser.UserId,
                CreatedDate = DateTime.Now,
            };
            _variantRepository.Add(variant);
            await _unitOfWork.SaveChangesAsync();
            return ResponseResult.Success($"create variant {variant.Name} successfully");
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw new VariantException.CreateVariantException(model.VariantName);
        }
    }

    public async Task<ResponseResult> UpdateVariant(UpdateVariantViewModel model, UserProfileModel currentUser)
    {

        var variant = await _variantRepository.FindByIdAsync(model.VariantId);
        if (variant == null)
        {
            throw new VariantException.VariantNotFoundException(model.VariantId);
        }

        var listImages = await _generalImageRepository.FindAll(s => model.ImageIds.Contains(s.Id))
            .Select(x => new ImageInEntity(x.Id, x.Name, x.Url)).ToListAsync();

        variant.Name = model.VariantName;
        variant.DiscountPrice = model.DiscountPrice;
        variant.Price = model.Price;
        variant.Quantity = model.Quantity;
        variant.ImageJson = HelperUtility.SerializeObject(listImages);
        try
        {
            _variantRepository.Update(variant);
            await _unitOfWork.SaveChangesAsync();
            return ResponseResult.Success($"update variant with id {variant.Id} successfully");
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw new VariantException.UpdateVariantException(variant.Id);
        }
    }

    public async Task<ResponseResult> DeleteVariant(Guid variantId)
    {

        var variant = await _variantRepository.FindByIdAsync(variantId);
        if (variant == null)
        {
            throw new VariantException.VariantNotFoundException(variantId);
        }

        try
        {
            
            _variantRepository.Remove(variant);
            await _unitOfWork.SaveChangesAsync();
            return ResponseResult.Success($"delete variant {variant.Name} successfully");
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw new VariantException.DeleteVariantException(variant.Id);
        }
    }


    public async Task<VariantViewModel> GetVariantById(Guid variantId)
    {
        var variant = await _variantRepository.FindByIdAsync(variantId);
        if (variant == null)
        {
            throw new VariantException.VariantNotFoundException(variantId);
        }
        
        var result = new VariantViewModel()
        {
            VariantId = variantId,
            VariantName = variant.Name,
            Price = variant.Price,
            DiscountPrice = variant.DiscountPrice,
            Status = variant.Status,
            Images = variant.ImageJson.ConvertToImageViewModel(_httpContextAccessor.HttpContext)
        };

        return result;
    }

    public async Task<ResponseResult> UpdateStatus(UpdateStatusViewModel model)
    {
        var item = await _variantRepository.FindByIdAsync(model.Id);
        if (item == null)
        {
            throw new VariantException.VariantNotFoundException(model.Id);
        }
        item.Status = model.Status;
        try
        {
            _variantRepository.Update(item);
            await _unitOfWork.SaveChangesAsync();
            return ResponseResult.Success();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw;
        }
       
    }


}

