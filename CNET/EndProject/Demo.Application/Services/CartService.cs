﻿using Demo.Domain;
using Demo.Domain.ApplicationServices.CartServices;
using Demo.Domain.ApplicationServices.Users;
using Demo.Domain.Entities;
using Demo.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Demo.Application.Services;

public class CartService : ICartService
{
    private readonly IGenericRepository<CartItem, Guid> _cartRepository;
    private readonly IGenericRepository<Variant, Guid> _variantRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly ILogger<CartService> _logger;
    public CartService(
        IGenericRepository<CartItem, Guid> cartRepository,
        IGenericRepository<Variant, Guid> variantRepository,
        IUnitOfWork unitOfWork, ILogger<CartService> logger)
    {
        _cartRepository = cartRepository;
        _variantRepository = variantRepository;
        _unitOfWork = unitOfWork;
        _logger = logger;
    }

    public async Task<CartPageResult<CartItemViewModel>> GetCart(UserProfileModel currentUser)
    {

        var items = await _cartRepository.FindAll(s => s.UserId == currentUser.UserId)
            .Select(s => new CartItemViewModel()
            {
                CartItemId = s.Id,
                UserId = s.UserId,
                VariantName = s.VariantName,
                Quantity = s.Quantity
            }).ToListAsync();
        var result = new CartPageResult<CartItemViewModel>(items);
        return result;
    }

    public async Task<ResponseResult> AddItemToCart(Guid variantId, UserProfileModel currentUser)
    {
        var variant = await _variantRepository.FindByIdAsync(variantId);
        if (variant == null)
        {
            throw new CartException.CartItemNotFoundException(variantId);
        }

        var newItem = new CartItem
        {
            Id = Guid.NewGuid(),
            UserId = currentUser.UserId,
            PurchasePrice = variant.DiscountPrice.HasValue ? variant.DiscountPrice.Value : variant.Price,
            Quantity = 1,
            CreatedBy = currentUser.UserId,
            CreatedDate = DateTime.Now
        };
        var currentItem = await _cartRepository.FindSingleAsync(s => s.UserId == currentUser.UserId && s.VariantId == variantId);

        try
        {
            if (currentItem == null)
            {
                _cartRepository.Add(newItem);
            }
            else
            {
                currentItem.Quantity += 1;
                currentItem.UpdatedBy = currentUser.UserId;
                currentItem.UpdatedDate = DateTime.Now;
                _cartRepository.Update(currentItem);
            }

            await _unitOfWork.SaveChangesAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw new CartException.ProcessCartException();
        }

        var cartItem = new CartItemViewModel()
        {
            CartItemId = newItem.Id,
            VariantName = newItem.VariantName,
            PurchasePrice = newItem.PurchasePrice,
            UserId = newItem.UserId,
            Quantity = newItem.Quantity,
        };
        return ResponseResult<CartItemViewModel>.Success(cartItem, $"add {newItem.VariantName} to cart success");
    }

    public async Task<ResponseResult> RemoveItemInCart(Guid variantId, UserProfileModel currentUser)
    {
        var currentItem = await _cartRepository.FindSingleAsync(s => s.UserId == currentUser.UserId && s.VariantId == variantId);
        if (currentItem == null)
        {
            throw new CartException.CartItemNotFoundException(variantId);
        }

        try
        {
            _cartRepository.Remove(currentItem);
            await _unitOfWork.SaveChangesAsync();
            var cartItem = new CartItemViewModel()
            {
                CartItemId = currentItem.Id,
                VariantName = currentItem.VariantName,
                PurchasePrice = currentItem.PurchasePrice,
                UserId = currentItem.UserId,
                Quantity = currentItem.Quantity,
            };
            return ResponseResult<CartItemViewModel>.Success(cartItem, $"remove {currentItem.VariantName} success");
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw new CartException.ProcessCartException();
        }
    }

    public async Task<ResponseResult> ChangeQuantity(ChangeQuantityViewModel model, UserProfileModel currentUser)
    {
        var currentItem = await _cartRepository.FindSingleAsync(s => s.UserId == currentUser.UserId && s.VariantId == model.VariantId);
        if (currentItem == null)
        {
            throw new CartException.CartItemNotFoundException(model.VariantId);
        }

        if (currentItem.Quantity < model.Quantity)
        {
            throw new CartException.OutOfStockException(currentItem.VariantName);
        }

        if (model.Quantity <= 0)
        {
            return await RemoveItemInCart(model.VariantId, currentUser);
        }

        currentItem.Quantity = model.Quantity;
        try
        {
            _cartRepository.Update(currentItem);
            await _unitOfWork.SaveChangesAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw new CartException.ProcessCartException();
        }

        var cartItem = new CartItemViewModel()
        {
            CartItemId = currentItem.Id,
            VariantName = currentItem.VariantName,
            PurchasePrice = currentItem.PurchasePrice,
            UserId = currentItem.UserId,
            Quantity = currentItem.Quantity,
        };
        return ResponseResult<CartItemViewModel>.Success(cartItem, $"update {currentItem.VariantName} quantity success");
    }

    public async Task<ResponseResult> ClearCart(UserProfileModel currentUser)
    {
        var items = await _cartRepository.FindAll(s => s.UserId == currentUser.UserId).AsTracking().ToListAsync();
        if (items == null || !items.Any())
        {
            return ResponseResult<bool>.Success(true, "clear cart successfully");
        }

        try
        {
            _cartRepository.RemoveMultiple(items);
            await _unitOfWork.SaveChangesAsync();
            return ResponseResult<bool>.Success(true, "clear cart successfully");
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message, e);
            throw new CartException.ProcessCartException();
        }
    }


}

