﻿using Demo.Domain;
using Demo.Domain.ApplicationServices.Images;
using Demo.Domain.ApplicationServices.Products;
using Demo.Domain.ApplicationServices.Users;
using Demo.Domain.Entities;
using Demo.Domain.Enums;
using Demo.Domain.Exceptions;
using Demo.Domain.Utility;
using DemoApp.Domain.Models.Variants;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Linq.Expressions;

namespace Demo.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGenericRepository<Product, Guid> _productRepository;
        private readonly IGenericRepository<GeneralImage, Guid> _generalImageRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<ProductService> _logger;

        public ProductService(
            IUnitOfWork unitOfWork,
            IGenericRepository<Product, Guid> productRepository,
            IGenericRepository<GeneralImage, Guid> generalImageRepository,
            IHttpContextAccessor httpContextAccessor, ILogger<ProductService> logger)
        {
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
            _generalImageRepository = generalImageRepository;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
        }
        public async Task<PageResult<ProductListingViewModel>> GetProductsForListingPage(ProductSearchQuery query)
        {
            var result = new PageResult<ProductListingViewModel>
            {
                CurrentPage = query.PageIndex
            };
            var productQuery = _productRepository.FindAll(null, s => s.Category, s => s.Variants, s => s.Reviews);
            if (query.MinPrice.HasValue)
            {
                productQuery = productQuery.Where(s => s.Variants.Min(x => x.Price) >= query.MinPrice);
            }

            if (query.MaxPrice.HasValue)
            {
                productQuery = productQuery.Where(s => s.Variants.Max(x => x.Price) <= query.MaxPrice);
            }

            if (query.DisplayActiveItem)
            {
                productQuery = productQuery.Where(s => s.Status == EntityStatus.Active);
            }

            if (!string.IsNullOrEmpty(query.Keyword))
            {
                productQuery = productQuery.Where(
                    s => s.Name.Contains(query.Keyword) || s.Category.Name.Contains(query.Keyword));
            }

            if (query.Categories != null && query.Categories.Any())
            {
                productQuery = productQuery.Where(s => query.Categories.Distinct().Contains(s.CategoryId));
            }

            productQuery = query.SortOrder == "desc"
          ? productQuery.OrderByDescending(GetSortProperty(query))
          : productQuery.OrderBy(GetSortProperty(query));

            result.TotalCount = await productQuery.CountAsync();

            productQuery = productQuery.Skip(query.SkipNo).Take(query.TakeNo);


            var products = await productQuery.Select(s => new ProductListingViewModel()
            {
                ProductName = s.Name,
                ImageJson = s.ImageJson,
                ProductId = s.Id,
                CategoryName = s.Category.Name,
                Price = s.Variants.Min(v => v.Price),
                DiscountPrice = s.Variants.Where(v => v.DiscountPrice.HasValue).Min(v => v.DiscountPrice.Value),
                Rating = s.Reviews.Max(s => s.Rating)

            }).ToListAsync();

            foreach (var product in products)
            {
                product.Images = product.ImageJson.ConvertToImageViewModel(_httpContextAccessor.HttpContext);
                product.ImageJson = string.Empty;

                product.DiscountPercent = product.DiscountPrice.HasValue ? (int)((product.Price - product.DiscountPrice.Value) / (product.Price)) : 0;
            }

            result.Data = products;
            return result;
        }

        public async Task<ProductDetailViewModel> GetProductForDetailPage(Guid productId)
        {
            var product = await _productRepository.FindByIdAsync
                (productId, s => s.Category, s => s.Variants, s => s.Reviews);
            if (product == null)
            {
                throw new ProductException.ProductNotFoundException(productId);
            }
            var productDetail = new ProductDetailViewModel
            {
                ProductId = product.Id,
                ProductName = product.Name,
                CategoryName = product.Category.Name,
                Reviews = product.Reviews.Select(r => new ReviewViewModel()
                {
                    ReviewerName = r.ReviewerName,
                    Email = r.Email,
                    Rating = r.Rating,
                    Content = r.Content

                }).ToList(),
                Variants = product.Variants.Select(s => new VariantViewModel()
                {
                    VariantId = s.Id,
                    VariantName = s.Name,
                    Price = s.Price,
                    DiscountPrice = s.DiscountPrice,
                    ImageJson = s.ImageJson
                }).ToList()
            };
            foreach (var variant in productDetail.Variants)
            {
                variant.Images = variant.ImageJson.ConvertToImageViewModel(_httpContextAccessor.HttpContext);
                variant.ImageJson = string.Empty;
            }

            return productDetail;
        }
        public async Task<ResponseResult> CreateProduct(CreateProductViewModel model, UserProfileModel currentUser)
        {
            await _unitOfWork.BeginTransactionAsync();
            try
            {
                var listImage = await _generalImageRepository.FindAll(s => model.ImageIds.Contains(s.Id))
                    .Select(s => new ImageInEntity(s.Id, s.Name, s.Url))
                    .ToListAsync();
                var product = new Product()
                {
                    Id = Guid.NewGuid(),
                    Name = model.ProductName,
                    Description = model.Description,
                    Detail = model.Detail,
                    CreatedBy = currentUser.UserId,
                    CreatedDate = DateTime.Now,
                    ImageJson = JsonConvert.SerializeObject(listImage)
                };
                _productRepository.Add(product);
                await _unitOfWork.SaveChangesAsync();


                await _unitOfWork.SaveChangesAsync();
                await _unitOfWork.CommitAsync();
                return ResponseResult.Success($"create product {product.Name} successfully");
            }
            catch (Exception e)
            {
                await _unitOfWork.RollbackAsync();
                _logger.LogError(e.Message, e);
                throw new ProductException.CreateProductException(model.ProductName);
            }
        }

        public async Task<ResponseResult> UpdateProduct(UpdateProductViewModel model, UserProfileModel currentUser)
        {

            var product = await _productRepository.FindByIdAsync(model.ProductId);
            if (product == null)
            {
                throw new ProductException.ProductNotFoundException(model.ProductId);
            }

            var listImage = await _generalImageRepository.FindAll(s => model.ImageIds.Contains(s.Id))
                .Select(s => new ImageInEntity(s.Id, s.Name, s.Url))
                .ToListAsync();

            product.Name = model.ProductName;
            product.Description = model.Description;
            product.Detail = model.Detail;
            product.CategoryId = model.CategoryId;
            product.ImageJson = JsonConvert.SerializeObject(listImage);

            try
            {
                _productRepository.Update(product);
                await _unitOfWork.SaveChangesAsync();
                return ResponseResult.Success($"update product with id {product.Id} successfully");
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                throw new ProductException.UpdateProductException(product.Id);
            }
        }

        public async Task<ResponseResult> UpdateStatus(UpdateStatusViewModel model)
        {
            var item = await _productRepository.FindByIdAsync(model.Id);
            if (item == null)
            {
                throw new ProductException.ProductNotFoundException(model.Id);
            }
            item.Status = model.Status;
            _productRepository.Update(item);
            await _unitOfWork.SaveChangesAsync();
            return ResponseResult.Success();
        }


        public async Task<ResponseResult> DeleteProduct(Guid productId)
        {

            var product = await _productRepository.FindByIdAsync(productId);
            if (product == null)
            {
                throw new ProductException.ProductNotFoundException(productId);
            }

            try
            {

                _productRepository.Remove(product);
                await _unitOfWork.SaveChangesAsync();
                return ResponseResult.Success($"delete variant {product.Name} successfully");
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, e);
                throw new VariantException.DeleteVariantException(product.Id);
            }
        }
        private Expression<Func<Product, object>> GetSortProperty(ProductSearchQuery query)
        => query.OrderBy?.ToLower() switch
        {
            "name" => product => product.Name,
            "date" => product => product.CreatedDate,
            _ => product => product.CreatedDate
        };

        private Expression<Func<Product, object>> GetSortProperty1(ProductSearchQuery query)
        {
            switch (query.OrderBy?.ToLower())
            {
                case "name":
                    return product => product.Name;
                case "date":
                    return product => product.CreatedDate;
                default:
                    return product => product.CreatedDate;
            }
        }

    }
}
