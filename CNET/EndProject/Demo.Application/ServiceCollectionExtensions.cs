﻿using Demo.Application.Services;
using Demo.Domain.ApplicationServices.CartServices;
using Demo.Domain.ApplicationServices.Categories;
using Demo.Domain.ApplicationServices.Images;
using Demo.Domain.ApplicationServices.Orders;
using Demo.Domain.ApplicationServices.Products;
using Demo.Domain.ApplicationServices.Users;
using Demo.Domain.ApplicationServices.Variants;
using Microsoft.Extensions.DependencyInjection;

namespace Demo.Application;

public static class ServiceCollectionExtensions
{
    public static void AddServicesApplication(this IServiceCollection services)
    {
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IImageService, ImageService>();
        services.AddScoped<ICategoryService, CategoryService>();
        services.AddScoped<IProductService, ProductService>();
        services.AddScoped<IVariantService, VariantService>();
        services.AddScoped<ICartService, CartService>();
        services.AddScoped<IOrderService, OrderService>();
    }

}