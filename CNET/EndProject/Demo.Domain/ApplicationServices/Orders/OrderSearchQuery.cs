﻿using Demo.Domain;

namespace Demo.Domain.ApplicationServices.Orders
{
    public class OrderSearchQuery : SearchQuery
    {
        public Guid? UserId { get; set; }
    }
}
