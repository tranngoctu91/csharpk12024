﻿using Demo.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Demo.Domain.ApplicationServices.Orders
{
    public class CreateOrderViewModel
    {
        [Required]
        public string ShippingAddress { get; set; } = string.Empty;
        [Required]
        public PaymentMethod PaymentMethod { get; set; }
    }
}
