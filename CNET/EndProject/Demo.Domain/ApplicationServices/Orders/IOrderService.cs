﻿using Demo.Domain.ApplicationServices.Users;

namespace Demo.Domain.ApplicationServices.Orders
{
    public interface IOrderService
    {
        Task<ResponseResult> PlaceOrder(CreateOrderViewModel model, UserProfileModel currentUser);

        Task<ResponseResult> UpdateOrderStatus(UpdateOrderStatusViewModel model);

        Task<PageResult<OrderViewModel>> GetOrders(OrderSearchQuery query);
    }
}
