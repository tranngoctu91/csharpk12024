﻿using Demo.Domain.Enums;

namespace Demo.Domain.ApplicationServices.Orders
{
    public class OrderViewModel
    {
        public Guid OrderId { get; set; }
        public Guid UserId { get; set; }
        public string ShippingAddress { get; set; } = string.Empty;
        public decimal TotalAmount { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

        public List<OrderDetailViewModel> OrderDetails { get; set; }

        public string OrderStatusName => OrderStatus.ToString();
        public string PaymentMethodsName => PaymentMethod.ToString();
    }

    public class OrderDetailViewModel
    {
        public Guid OrderDetailId { get; set; }
        public Guid UserId { get; set; }

        public string VariantName { get; set; }

        public decimal PurchasePrice { get; set; }

        public int Quantity { get; set; }

        public Guid VariantId { get; set; }

        public Guid OrderId { get; set; }

    }
}
