﻿using Demo.Domain.Enums;

namespace Demo.Domain.ApplicationServices.Orders
{
    public class UpdateOrderStatusViewModel
    {
        public OrderStatus OrderStatus { get; set; }
        public Guid OrderId { get; set; }
    }
}
