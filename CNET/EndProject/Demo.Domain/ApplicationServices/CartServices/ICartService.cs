﻿using Demo.Domain.ApplicationServices.Users;

namespace Demo.Domain.ApplicationServices.CartServices
{
    public interface ICartService
    {
        Task<CartPageResult<CartItemViewModel>> GetCart(UserProfileModel currentUser);
        Task<ResponseResult> AddItemToCart(Guid variantId, UserProfileModel currentUser);
        Task<ResponseResult> RemoveItemInCart(Guid variantId, UserProfileModel currentUser);
        Task<ResponseResult> ChangeQuantity(ChangeQuantityViewModel model, UserProfileModel currentUser);
        Task<ResponseResult> ClearCart(UserProfileModel currentUser);
    }
}
