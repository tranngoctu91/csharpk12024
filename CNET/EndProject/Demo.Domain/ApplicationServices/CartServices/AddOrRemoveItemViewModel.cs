﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Domain.ApplicationServices.CartServices
{
    public class AddOrRemoveItemViewModel
    {
        [Required]
        public Guid VariantId { get; set; }
    }
}
