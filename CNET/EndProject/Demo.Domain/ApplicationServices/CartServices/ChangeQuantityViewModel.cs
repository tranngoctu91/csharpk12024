﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Domain.ApplicationServices.CartServices
{
    public class ChangeQuantityViewModel
    {
        [Required]
        public Guid VariantId { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}
