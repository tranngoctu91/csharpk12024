﻿namespace Demo.Domain.ApplicationServices.CartServices
{
    public class CartItemViewModel
    {
        public Guid  CartItemId { get; set; }
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public decimal PurchasePrice { get; set; }
        public Guid UserId { get; set; }
        public Guid Id { get; set; }
        public int Quantity { get; set; }
    }
}
