﻿namespace Demo.Domain.ApplicationServices.CartServices
{
    public class CartPageResult<CartViewModel> : PageResult<CartItemViewModel> 
    {
        public decimal TotalAmount { get; set; }
        public CartPageResult(List<CartItemViewModel> items)
        {
            TotalCount= items.Count;
            TotalAmount = items.Sum(s => s.Quantity * s.PurchasePrice);
            Data = items;
        }
    }
}
