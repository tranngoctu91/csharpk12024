﻿using Demo.Domain.ApplicationServices.Images;
using Newtonsoft.Json;

namespace Demo.Domain.ApplicationServices.Products
{
    public class ProductListingViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? DiscountPrice { get; set; }
        public int? DiscountPercent { get; set; }
        public decimal? Price { get; set; }
        public int? Rating { get; set; }
        public List<ImageViewModel>? Images { get; set; }

        [JsonIgnore]
        public string ImageJson { get; set; }
        public string CategoryName { get; set; }
    }
}
