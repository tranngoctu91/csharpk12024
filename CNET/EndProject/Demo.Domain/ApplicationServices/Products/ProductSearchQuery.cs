﻿namespace Demo.Domain.ApplicationServices.Products
{
    public class ProductSearchQuery : SearchQuery
    {
        public List<Guid>? Categories { get; set; }
        public decimal? MaxPrice { get; set; }
        public decimal? MinPrice { get; set; }
        public string OrderBy { get; set; }
        public string SortOrder { get; set; }
    }
}
