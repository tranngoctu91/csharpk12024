﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Domain.ApplicationServices.Products
{
    public class ReviewViewModel
    {
        public string ReviewerName { get; set; }
        public string? Email { get; set; }
        public string? Content { get; set; }
        public int Rating { get; set; }
    }
}
