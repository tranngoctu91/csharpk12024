﻿using DemoApp.Domain.Models.Variants;

namespace Demo.Domain.ApplicationServices.Products
{
    public class ProductDetailViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? DiscountPrice { get; set; }
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public string CategoryName { get; set; }

        public List<ReviewViewModel> Reviews { get; set; }
        public List<VariantViewModel> Variants { get; set; }
    }
}
