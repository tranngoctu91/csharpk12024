﻿namespace Demo.Domain.ApplicationServices.Products
{
    public class UpdateProductViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string? Detail { get; set; }
        public string? Description { get; set; }
        public Guid CategoryId { get; set; }
        public List<Guid> ImageIds { get; set; }
    }
}
