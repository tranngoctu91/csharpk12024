﻿using Demo.Domain.ApplicationServices.Users;

namespace Demo.Domain.ApplicationServices.Products
{
    public interface IProductService
    {
        Task<PageResult<ProductListingViewModel>> GetProductsForListingPage(ProductSearchQuery query);
        Task<ProductDetailViewModel> GetProductForDetailPage(Guid id);

        Task<ResponseResult> UpdateStatus(UpdateStatusViewModel model);
        Task<ResponseResult> CreateProduct(CreateProductViewModel model, UserProfileModel currentUser);
        Task<ResponseResult> UpdateProduct(UpdateProductViewModel model, UserProfileModel currentUser);
        Task<ResponseResult> DeleteProduct(Guid productId);
    }
}
