﻿namespace Demo.Domain.ApplicationServices.Products
{
    public class CreateProductViewModel
    {
        public string ProductName { get; set; }
        public string? Detail { get; set; }
        public string? Description { get; set; }
        public List<Guid> ImageIds { get; set; }
    }
}
