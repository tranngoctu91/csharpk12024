﻿using Demo.Domain.ApplicationServices.Users;
using DemoApp.Domain.Models.Variants;

namespace Demo.Domain.ApplicationServices.Variants
{
    public interface IVariantService
    {
        Task<ResponseResult> CreateVariant(CreateVariantViewModel model, UserProfileModel currentUser);
        Task<ResponseResult> UpdateVariant(UpdateVariantViewModel model, UserProfileModel currentUser);
        Task<ResponseResult> DeleteVariant(Guid variantId);
        Task<VariantViewModel> GetVariantById(Guid variantId);

        Task<ResponseResult> UpdateStatus(UpdateStatusViewModel model);
    }
}
