﻿using Demo.Domain.ApplicationServices.Images;
using Demo.Domain.Enums;
using Newtonsoft.Json;

namespace DemoApp.Domain.Models.Variants
{
    public class VariantViewModel
    {
        public Guid VariantId { get; set; }
        public string VariantName { get; set; }
        public decimal? DiscountPrice { get; set; }
        public decimal Price { get; set; }
        public EntityStatus Status { get; set; }
        public List<ImageViewModel>? Images { get; set; }

        [JsonIgnore]
        public string ImageJson { get; set; }

    }

    
}
