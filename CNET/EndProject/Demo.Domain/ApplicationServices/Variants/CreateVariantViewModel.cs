﻿using System.ComponentModel.DataAnnotations;

namespace Demo.Domain.ApplicationServices.Variants
{
    public class CreateVariantViewModel
    {
        [Required]
        public string VariantName { get; set; }
        public decimal Price { get; set; }
        public decimal? DiscountPrice { get; set; }
        public int Quantity { get; set; } = 0;
        public Guid ProductId { get; set; }
        public  List<Guid> ImageIds { get; set; }
    }
}
