﻿using Microsoft.AspNetCore.Mvc;

namespace DemoApp.Api.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class NoAuthorizeController : ControllerBase
    {

    }
}
