﻿using Demo.Api.Base;
using Demo.Api.Filters;
using Demo.Domain;
using Demo.Domain.ApplicationServices.Variants;
using Demo.Domain.Utility;
using Microsoft.AspNetCore.Mvc;

namespace DemoApp.Api.Controllers.Management
{

    public class AdminVariantController : AuthorizeController
    {
        private readonly IVariantService _variantService;
        public AdminVariantController(IVariantService variantService)
        {
            _variantService = variantService;
        }

        [Permission(CommonConstants.Permissions.ADD_VARIANT_PERMISSION)]
        [ValidateModelState]
        [HttpPost]
        [Route("create-variant")]
        public async Task<ResponseResult> CreateVariant([FromForm] CreateVariantViewModel model)
        {
           
            var result = await _variantService.CreateVariant(model, CurrentUser);
            return result;
        }

        [Permission(CommonConstants.Permissions.UPDATE_VARIANT_PERMISSION)]
        [ValidateModelState]
        [HttpPut]
        [Route("update-variant")]
        public async Task<ResponseResult> UpdateVariant([FromForm] UpdateVariantViewModel model)
        {
            var result = await _variantService.UpdateVariant(model, CurrentUser);
            return result;
        }

        [Permission(CommonConstants.Permissions.DELETE_VARIANT_PERMISSION)]
        [HttpDelete]
        [Route("delete-variant/{categoryId}")]
        public async Task<ResponseResult> DeleteVariant(Guid variantId)
        {
            var result = await _variantService.DeleteVariant(variantId);
            return result;
        }

        [Permission(CommonConstants.Permissions.UPDATE_VARIANT_PERMISSION)]
        [ValidateModelState]
        [HttpDelete]
        [Route("update-variant-status")]
        public async Task<ResponseResult> UpdateStatus([FromBody] UpdateStatusViewModel model)
        {
            var result = await _variantService.UpdateStatus(model);
            return result;
        }
    }
}
