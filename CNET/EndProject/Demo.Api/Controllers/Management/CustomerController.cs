﻿using Demo.Api.Base;
using Demo.Domain;
using Demo.Domain.ApplicationServices.CartServices;
using Demo.Domain.ApplicationServices.Orders;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.Management
{

    public class CustomerController : AuthorizeController
    {
        private readonly IOrderService _orderService;
        private readonly ICartService _cartService;

        public CustomerController(IOrderService orderService, ICartService cartService)
        {
            _orderService = orderService;
            _cartService = cartService;
        }


        [HttpPost]
        [Route("get-orders-customer")]
        public async Task<PageResult<OrderViewModel>> GetOrders([FromBody] OrderSearchQuery query)
        {
            query.UserId = CurrentUser.UserId;
            var result = await _orderService.GetOrders(query);
            return result;
        }

        [HttpPost]
        [Route("place-order")]
        public async Task<ResponseResult> PlaceOrder([FromBody] CreateOrderViewModel model)
        {

            var result = await _orderService.PlaceOrder(model, CurrentUser);
            return result;
        }

        [HttpPost]
        [Route("get-cart")]
        public async Task<PageResult<CartItemViewModel>> GetCart()
        {
            var result = await _cartService.GetCart(CurrentUser);
            return result;
        }

        [HttpPost]
        [Route("add-item-to-cart")]
        public async Task<ResponseResult> AddItem([FromBody] AddOrRemoveItemViewModel model)
        {
            var result = await _cartService.AddItemToCart(model.VariantId, CurrentUser);
            return result;
        }


        [HttpPost]
        [Route("remove-item-in-cart")]
        public async Task<ResponseResult> RemoveItem([FromBody] AddOrRemoveItemViewModel model)
        {
            var result = await _cartService.RemoveItemInCart(model.VariantId, CurrentUser);
            return result;
        }

        [HttpPost]
        [Route("change-item-quantity")]
        public async Task<ResponseResult> ChangeItemQuantity([FromBody] ChangeQuantityViewModel model)
        {
            var result = await _cartService.ChangeQuantity(model, CurrentUser);
            return result;
        }

    }
}
