﻿using Demo.Api.Base;
using Demo.Api.Filters;
using Demo.Domain;
using Demo.Domain.ApplicationServices.Products;
using Demo.Domain.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.Management
{

    public class AdminProductController : AuthorizeController
    {
        private readonly IProductService _productService;
        public AdminProductController(IProductService productService)
        {
            _productService = productService;
        }

        [Permission(CommonConstants.Permissions.ADD_PRODUCT_PERMISSION)]
        //[ValidateModelState]
        [HttpPost]
        [Route("create-product")]
        public async Task<ResponseResult> CreateProduct([FromBody] CreateProductViewModel model)
        {
            var result = await _productService.CreateProduct(model, CurrentUser);
            return result;
        }

        [Permission(CommonConstants.Permissions.UPDATE_PRODUCT_PERMISSION)]
        //[ValidateModelState]
        [HttpPut]
        [Route("update-product")]
        public async Task<ResponseResult> UpdateProduct([FromBody] UpdateProductViewModel model)
        {
            var result = await _productService.UpdateProduct(model, CurrentUser);
            return result;
        }

        [Permission(CommonConstants.Permissions.DELETE_PRODUCT_PERMISSION)]
        [HttpDelete]
        [Route("delete-product/{productId}")]
        public async Task<ResponseResult> DeleteProduct(Guid productId)
        {
            var result = await _productService.DeleteProduct(productId);
            return result;
        }

        [Permission(CommonConstants.Permissions.UPDATE_PRODUCT_PERMISSION)]
        [HttpDelete]
        [Route("update-product-status")]
        public async Task<ResponseResult> UpdateStatus([FromBody] UpdateStatusViewModel model)
        {
            var result = await _productService.UpdateStatus(model);
            return result;
        }
    }
}
