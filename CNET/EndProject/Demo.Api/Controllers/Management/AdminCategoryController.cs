﻿using Demo.Api.Base;
using Demo.Api.Filters;
using Demo.Domain;
using Demo.Domain.ApplicationServices.Categories;
using Demo.Domain.InfrastructureServices;
using Demo.Domain.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.Management
{

    public class AdminCategoryController : AuthorizeController
    {
        private readonly ICategoryService _categoryService;
        private readonly ICacheService _cacheService;

        public AdminCategoryController(ICategoryService CategoryService, ICacheService cacheService)
        {
            _categoryService = CategoryService;
            _cacheService = cacheService;
        }

        [Permission(CommonConstants.Permissions.ADD_CATEGORY_PERMISSION)]
        [HttpPost]
        [Route("create-category")]
        public async Task<ResponseResult> CreateCategory([FromBody] CreateCategoryViewModel model)
        {
            var result = await _categoryService.CreateCategory(model, CurrentUser);
            await _cacheService.RemoveAsync("cache-cate");
            return result;
        }

        [Permission(CommonConstants.Permissions.UPDATE_CATEGORY_PERMISSION)]
        [HttpPut]
        [Route("update-category")]
        public async Task<ResponseResult> UpdateCategory([FromBody] UpdateCategoryViewModel model)
        {
            var result = await _categoryService.UpdateCategory(model, CurrentUser);
            await _cacheService.RemoveAsync("cache-cate");
            return result;
        }

        [Permission(CommonConstants.Permissions.DELETE_CATEGORY_PERMISSION)]
        [HttpDelete]
        [Route("delete-category/{categoryId}")]
        public async Task<ResponseResult> DeleteCategory(Guid categoryId)
        {
            var result = await _categoryService.DeleteCategory(categoryId);
            await _cacheService.RemoveAsync("cache-cate");
            return result;
        }

        [Permission(CommonConstants.Permissions.UPDATE_CATEGORY_PERMISSION)]
        [HttpDelete]
        [Route("update-category-status")]
        public async Task<ResponseResult> UpdateStatus([FromBody] UpdateStatusViewModel model)
        {
            var result = await _categoryService.UpdateStatus(model);
            await _cacheService.RemoveAsync("cache-cate");
            return result;
        }
    }
}
