﻿using Demo.Api.Filters;
using Demo.Domain;
using Demo.Domain.ApplicationServices.Users;
using Demo.Domain.Utility;
using DemoApp.Api.Controllers.Base;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.Public
{
    public class NoAuthController: NoAuthorizeController
    {
        private readonly IUserService _userService;
        private readonly ILogger<NoAuthController> _logger;
        public NoAuthController(IUserService userService, ILogger<NoAuthController> logger)
        {
            _userService = userService;
            _logger = logger;
        }
        [HttpPost]
        [Route("login")]
        public async Task<AuthorizedResponseModel> Login([FromBody] LoginViewModel model)
        {
            _logger.LogError("TNT-LOGIN");
            var result = await _userService.Login(model);
            return result;
        }

        [HttpPost]
        [Route("register-customer")]
        public async Task<ResponseResult> RegisterCustomer([FromBody] RegisterUserViewModel model)
        {
            var result = await _userService.RegisterCustomer(model);
            return result;
        }
    }
}
