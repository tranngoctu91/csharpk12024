﻿using Demo.Domain;
using Demo.Domain.ApplicationServices.Products;
using DemoApp.Api.Controllers.Base;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.Public
{

    public class ProductController : NoAuthorizeController
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [HttpPost]
        [Route("get-products")]
        public async Task<PageResult<ProductListingViewModel>> GetProducts([FromBody] ProductSearchQuery query)
        {
            var result = await _productService.GetProductsForListingPage(query);
            return result;
        }

        [HttpGet]
        [Route("get-product-detail/{productId}")]
        public async Task<ProductDetailViewModel> GetProductDetail(Guid productId)
        {
            var result = await _productService.GetProductForDetailPage(productId);
            return result;
        }
    }
}
