﻿using Demo.Domain;
using Demo.Domain.ApplicationServices.Categories;
using Demo.Domain.InfrastructureServices;
using DemoApp.Api.Controllers.Base;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.Public
{

    public class CategoryController : NoAuthorizeController
    {
        private readonly ICategoryService _categoryService;
        private readonly ICacheService _cacheService;

        public CategoryController(ICategoryService categoryService, ICacheService cacheService)
        {
            _categoryService = categoryService;
            _cacheService = cacheService;
        }


        [HttpPost]
        [Route("get-categories")]
        public async Task<PageResult<CategoryViewModel>> GetProducts([FromBody] CategorySearchQuery query)
        {
            var result = new PageResult<CategoryViewModel>();
            var cacheResult = await _cacheService.GetAsync<PageResult<CategoryViewModel>>("cache-cate");
            if (cacheResult != null)
            {
                result = cacheResult;
            }
            else
            {
                result = await _categoryService.GetCategories(query);
                await _cacheService.SetAsync("cache-cate", result);
            }
            return result;
        }

        [HttpGet]
        [Route("get-category/{categoryId}")]
        public async Task<CategoryViewModel> GetProducts(Guid categoryId)
        {
            var result = await _categoryService.GetCategoryById(categoryId);
            return result;
        }

    }
}
