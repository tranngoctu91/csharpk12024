﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace LearnWhileDoWhile
{
	public class Demo
	{
		// viết chương trình yêu cầu người dùng nhập tên,sau khi nhập xong thì in ra, và hỏi có muốn nhập hay ko, nếu muốn nhập tiếp, ko => dừng lại
		public void EasyWhile(int number)
		{
			int i = 0;
			while (i <= number)
			{
				if (i % 2 != 0)
				{
					Console.WriteLine(i);
				}
				i++;
			}
		}

		public void EasyInputStudent()
		{
			var isContinue = true;
			while (isContinue)
			{
				Console.WriteLine("input the name:");
				var name = Console.ReadLine();
				Console.WriteLine($"your name: {name}");

				Console.WriteLine("Do you want to continue? (y/n)");
				isContinue = Console.ReadLine() == "y";
			}
			return;
		}

		public void EasyValidateAge()
		{
			Console.WriteLine("input the age");
			var check = int.TryParse(Console.ReadLine(), out var age) && age > 0;
			while (!check)
			{
				Console.WriteLine("the age is not correct, reinput");
				check = int.TryParse(Console.ReadLine(), out age) && age > 0;
			}
			Console.WriteLine($"the age: {age}");
		}


		public void InputStudent()
		{
			var isContinue = true;
			while (isContinue)
			{
				Console.WriteLine("Input the name");
				var name = Console.ReadLine();
				Console.WriteLine("input the age");
				var checkAge = int.TryParse(Console.ReadLine(), out var age) && age >= 12 && age <= 15;
				while (!checkAge)
				{
					Console.WriteLine("the age is not correct, reinput");
					checkAge = int.TryParse(Console.ReadLine(), out age) && age > 0;
				}

				var math = ValidateScore("math");
				var literature = ValidateScore("literature");
				var english = ValidateScore("english");

				Console.WriteLine($"name: {name}, age: {age}, avg score: {(math + literature + english) / 3}");

				Console.WriteLine("Do you want to continue? (y/n)");
				isContinue = Console.ReadLine() == "y";
			}
		}
		public double ValidateScore(string scoreName)
		{
			Console.WriteLine($"input the {scoreName} score");
			var checkScore = double.TryParse(Console.ReadLine(), out var score) && score > 0 && score <= 10;
			while (!checkScore)
			{
				Console.WriteLine($"the {scoreName} is not correct, reinput");
				checkScore = double.TryParse(Console.ReadLine(), out score) && score > 0 && score <= 10;
			}
			return score;
		}

		public void DoWhile()
		{
			int number = 5;
			do
			{
				Console.WriteLine(number);
			} while (number < 5);
		}
	}
}
