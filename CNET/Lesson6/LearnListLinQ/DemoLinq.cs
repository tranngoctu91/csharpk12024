﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnListLinQ
{
	public class DemoLinq
	{
		public List<int> numbers = new List<int>() { 10, 5, 4, 3, 50, 30, 7, 8, 4, 15, 13 };


		public void AllAny()
		{
			var result = numbers.Any(s => s % 2 != 0);
            Console.WriteLine(result);
        }
		//inra số lẻ
		public void PrintOddNumbers()
		{
			//var result = numbers.Where(x => x % 2 != 0).ToList();

			var result = numbers.Where(x => x % 2 != 0).OrderByDescending(x => x).ToList();
			PrintResult(result);
		}
		public void PrintSelect()
		{
			var result = numbers.Select(s => s * 2).ToList();
			PrintResult(result);
		}

		public void TakeSkip()
		{
			var result = numbers.Skip(3).Take(3).ToList();
			PrintResult(result);
		}
		public void PrintSelect1()
		{
			var result = numbers.Select(s => $"{s} is {GetNumber(s)}").ToList();
			PrintResult(result);
		}

		private string GetNumber(int number) => number % 2 == 0 ? "so chan" : "sole";
		public void PrintOne()
		{
			var firstElement = numbers.Where(x => x % 2 != 0).OrderBy(x => x).FirstOrDefault();
			Console.WriteLine(firstElement);
		}
		public void PrintResult(List<int> result)
		{
			if (!result.Any())
			{
                Console.WriteLine("result is empty");
				return;
            }
			foreach (int i in result)
			{
				Console.WriteLine(i);
			}
		}

		public void PrintResult(List<string> result)
		{
			foreach (string i in result)
			{
				Console.WriteLine(i);
			}
		}
	}
}
