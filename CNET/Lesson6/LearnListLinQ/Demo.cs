﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnListLinQ
{
	public class Demo
	{
		public void EasyList()
		{
			var numbers = new List<int>();
			numbers.Add(1);
			numbers.Add(2);
			numbers.Add(3);
			numbers.Add(5);
			var firstElement = numbers[0];
			var lastElement = numbers[numbers.Count - 1];
			numbers.Remove(firstElement);
		}
		// viết 1 ứng dụng từ điển:
		//1 list các từ tiếng việt : bàn, ghế, sách..
		// 1 list các từ tiếng anh: table,chair, book
		// nhập 1 từ tiếng anh =>từ tiếng việt tương ứng, nếu ko có thì trả ra là ko tìm thấy

		public void Dictionary()
		{
			var isContinue = true;
			while (isContinue)
			{
				Console.OutputEncoding = Encoding.UTF8;
				Console.InputEncoding = Encoding.UTF8;
				var engs = new List<string>() { "table", "chair", "book", "suitable" };
				var vns = new List<string>() { "bàn", "ghế", "sách", "phù hợp" };
				Console.WriteLine("Nhập từ cần tra");
				var word = Console.ReadLine();
				var result = LookUp1(word, engs, vns);
				Console.WriteLine(result);
				Console.WriteLine("Bạn có muốn tiếp tục? (y/n)");
				isContinue = Console.ReadLine().Equals("y", StringComparison.OrdinalIgnoreCase);
			}
		}

		private string LookUp(string word, List<string> engs, List<string> vns)
		{
			for (int i = 0; i < engs.Count; i++)
			{
				//if (word == engs[i])
				if (word.Equals(engs[i], StringComparison.OrdinalIgnoreCase))
				{
					return $"Kết quả là:{vns[i]}";
				}
			}
			return "Không tìm thấy";
		}

		private string LookUp1(string word, List<string> engs, List<string> vns)
		{
			var rerult = new List<string>();
			for (int i = 0; i < engs.Count; i++)
			{
				//if (word == engs[i])
				if (engs[i].Contains(word, StringComparison.OrdinalIgnoreCase))
				{
					rerult.Add($"{engs[i]} : {vns[i]}");
				}
			}
			if (rerult.Count > 0)
			{
				string disPlayResult = "Kết quả là: ";
				foreach (var item in rerult)
				{
					disPlayResult += item + ";";
				}
				return disPlayResult;
			}
			else
			{
				return "Không tìm thấy";
			}
			
		}


	}
}
