﻿// See https://aka.ms/new-console-template for more information
using LearnArray;

Console.WriteLine("Hello, World!");
int[] numbers = new int[5];
//int[] numbers1 = new int[5] { 1, 5, 6, 7, 8 };
//int[] numbers2 = { 1, 5, 6, 7, 8 };

numbers[0] = 10;
numbers[1] = 20;
numbers[2] = 30;
numbers[3] = 40;
numbers[4] = 50;
var firstElement = numbers[0];

var lastElement = numbers[numbers.Length - 1];
// viết chương trình cho phép định ra chiều dài mảng, sau đó nhập phần tử vào mảng số nguyên, sau đó tính giá trị trung bình cac phần tử trong mảng.
var length = numbers.Length;


Demo demo =new Demo();
demo.InputArray();
Console.ReadKey();